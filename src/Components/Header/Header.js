import React from 'react';
import { Link } from 'react-router-dom';
import {faHome} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Header = () => {
    return (
        <header className="header">

                <Link to="/"><FontAwesomeIcon icon={faHome} /> HOME </Link>< br/>
                <Link to="/about"><FontAwesomeIcon icon={faHome} /> ABOUT US </Link>

        </header>
    );
};

export default Header;