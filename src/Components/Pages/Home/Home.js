import React from 'react';
import Header from "../../Header/Header";
import Slider from "./Slider/Slider";
import WhyChooseUs from "./WhyChooseUs/WhyChooseUs";

const Home = () => {
    return (
        <section className="text-home">
            <h1>Это страница HOME</h1>
            <Slider />
            <WhyChooseUs />
        </section>
    );
};

export default Home;