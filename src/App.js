import { Routes, Route, Link } from 'react-router-dom';

import Home from './Components/Pages/Home/Home';
import AboutUs from './Components/Pages/AboutUs/AboutUs';

import './scss/main.scss';


import Header from "./Components/Header/Header";


function App() {
  return (
      <>
          <Header />
          <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/about" element={<AboutUs />} />
          </Routes>

      </>
  );
}

export default App;